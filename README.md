# Scala crawler sample app #

just  
`sbt run`  
- OR -  
`sbt run [IDs FILE]`  
- OR -  
`sbt run itemIds.txt`  



![mood.jpeg](https://bitbucket.org/repo/BxnE7X/images/2773784865-mood.jpeg)

# Description #

a Scala program which scrapes descriptions for apps in the iTunes App Store, and find the most important words for each description.

Concretely, your program should take as input a file with iTunes IDs. For example, here are ten lines in the file:

429775439  
553834731  
529479190  
499511971  
513474544  

(you can assume the input is well formatted, no need to check for aberrations etc)

It should call 
http://itunes.apple.com/lookup?id=ID

For example
http://itunes.apple.com/lookup?id=429775439 (go ahead and try this link)

Extract the description from the response.  
What we want is to calculate the TF-IDF score http://en.wikipedia.org/wiki/Tf–idf for each word in each app description, displaying the top 10 words per app sorted by their TF-IDF score (like this:

123456: fantastic 4.513, game 2.414, iPhone 2.1313, ...
84282489: whatever 8.7241, tap 7.434, iPhone 4.2626...
...
)

The "corpus" for TF-IDF purposes is the list of all app descriptions.



Current Todos
*  ~~ swap scala's json with json4s or something nicer ~~
*  ~~ break long lines and wrap ~~
*  ~~ reduce those asInstaceOf ~~
