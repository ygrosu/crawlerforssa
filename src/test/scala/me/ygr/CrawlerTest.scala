/**
 * Created by yairgrosu on 7/30/14.
 */
package me.ygr


import org.scalatest._
import org.scalatest.exceptions.TestFailedException


import scala.collection.immutable.{Map=>MyMap}
import scala.util.parsing.json.{JSON, JSONArray, JSONObject}

object CrawlerTest {
    val dogString = Option("a big dog went home to like other dog and chase cat that runs from dog. The dog's name is Freddy and it is a pet.")

    val catString = Option("a small cat went to check the mice. It ran away when it rained and the pet was pretty cool. ")

    val birdString = Option("i like my little bird. it is so cure and it is a nice bird. where is the Bird")

    val mediumDoc = Option( """A pet store or pet shop is a retail business which sells different kinds of animals to the public. A variety of animal supplies and pet accessories are also sold in pet shops. The products sold include: food, treats, toys, collars, leashes, cat litter, cages and aquariums. Some pet stores provide engraving services for pet tags, which have the owner’s contact information in case the pet gets lost.
                              |In the USA and Canada, pet shops often offer both hygienic care (such as pet cleaning) and esthetic services (such as cat and dog grooming). Grooming is the process by which a dog or cats's physical appearance is enhanced and kept according to breed standards for competitive breed showing, for other types of competition, like creative grooming or pet tuning contests, or just to their owners taste. Some pet stores also provide tips on training and behaviour, as well as advice on pet nutrition.
                              |There are many large pet stores located in the US and Canada, including: Petland, Pet Valu, and PetSmart. In the United States, Petco is also a popular pet store. In addition, there are many smaller pet shops that aren't part of big chains, such as Big Al’s, which have a smaller number of locations.""")

    val sourceCorpus = Map(100 -> dogString)
}
import CrawlerTest._
class CrawlerTest extends FlatSpec with Matchers {

    "The short document " should " have 3 times the term dog" in {
        val docAnalyzer = EngDocAnalyzer.analyzer()
        val dogHist = docAnalyzer.analyze(CrawlerTest.dogString, 100)._2
        dogHist("dog") should be(4)
    }

    "The analyzer " should " adapt stemming mode with objects" in {
        val trimMe = StemTrim
        val lowerMe = StemLower


        val refStrings = List("aBcD", "  Dog123  aAAa", "112 ALL UPPER  ")
        val refLowed = List("abcd", "  dog123  aaaa", "112 all upper  ")
        val refLowedTrimmed = List("abcd", "dog123  aaaa", "112 all upper")
        val refTrimmed = List("aBcD", "Dog123  aAAa", "112 ALL UPPER")

        val testArray = List(("lowed", refLowed, List(lowerMe)), ("trimmed", refTrimmed, List(trimMe)), ("lowed,trimmed", refLowedTrimmed, List(lowerMe, trimMe)), ("trimmed,lowed", refLowedTrimmed, List(trimMe, lowerMe)))

        testArray.foreach({ case (testLabel, ref, opsList) => {
            val analyzer = new DocAnalyzer(opsList,"")
            try {
                refStrings.map(aWord => analyzer.stemTheWord(aWord)) should be(ref)
            } catch {
                case tfe: TestFailedException => fail(s"Failed reading for semming type:$testLabel, the original is\n${tfe.getMessage()}")
            }
        }})
    }


    val jsonDogString =
        s"""
          |{
          |     "resultCount": 1,
          |     "results": [
          |     {
          |         "kind": "software",
          |         "description": "${dogString.get}",
          |         "descript222": "#1 in 116 countries! B"
          |     }
          |     ]
          |}
        """.stripMargin




    "And now for something totally diffrent ... " should " this work ? " in{

        val firstDog = 1111
        val secondDog = 4444

        val theList = Map(firstDog->dogString, 2222->catString, 3333->birdString, secondDog-> Crawler.descFromJson(jsonDogString))
        val docAnalyzer = EngDocAnalyzer.analyzer()

        val corpus: Map[Long, Map[String, Int]] = theList.map({case (k,v) => docAnalyzer.analyze(v,k) }).toList.toMap

        corpus(firstDog) shouldEqual corpus(secondDog)

        getTheAnalyzedDocs(corpus)

    }


}

