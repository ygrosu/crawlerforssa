package me

import java.io.StringWriter

import scala.collection.parallel.immutable.ParMap
import scala.io.Source
import scala.text.Document

/**
 * Created by yairgrosu on 7/30/14.
 *
 *
 *
 */

package object ygr extends DocIndexer with TfAlgorithmAugmented{
    val jsonUrlPfx = "http://itunes.apple.com/lookup?id="
    val itemIds = List(429775439, 553834731, 529479190, 499511971, 513474544,
        506627515, 504720040, 447553564, 440045374, 512939461)

    def main(args: Array[String]): Unit = {
        println("Hi there, here we go!")

        val theList = args.length match{
            case 0 => {println ("using the internal sample IDs, enjoy.  You can add the path to file with ids");itemIds}
            case _ => {
                println(s"Taking IDs from the file at [$args(0)]")
                val ids = Source.fromFile(args(0)).getLines().map(line => line.toInt).toList
                println(s"Found ${ids.length} ids in the file, let's go")
                ids
            }
        }

        val corpus : Map[Long, Map[String, Int]] = theList.par.map(item => Crawler.processDocFromUrl(jsonUrlPfx, item)).toList.toMap

        getTheAnalyzedDocs(corpus)
    }



    def getTheAnalyzedDocs(corpus: Map[Long, Map[String, Int]]) = {
        val docsTfs = corpus.map( {case (docId, termsMap) => { (docId-> processDocTf(docId, termsMap) ) } }  ).toMap

        val corpusIdf = processCorpusIdf(corpus.values.toList)


        val docsTermScores = docsTfs.map({case (docId, tfsMap) => { (docId-> docTermsScores(tfsMap, corpusIdf) ) } }  ).toMap


        val docsToTerms = docsTermScores.map({case (docId, termScores) => { (docId->termScores.toSeq.sortBy(_._2*(-1))) } })

        println ("Et voila ! \n\nRated documents coming")


        docsToTerms.foreach({case(id, doc) => println(s"$id) ${doc.take(10)}\n")})

        println("=====\nAnd the corpus")

//        corpus.keySet.foreach(key => println(s"$key)  ${corpus(key)}") )

        println("\n.... and we are done.")

        val docId = List(553834731,506627515)
        docsTermScores
        List(553834731,506627515).map(docId=>println(s"\n\n$docId == ${docsTermScores(docId).toSeq.sortBy(_._2)}"))

    }
}
