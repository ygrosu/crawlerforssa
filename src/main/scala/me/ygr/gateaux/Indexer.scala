package me.ygr.gateaux


import scala.collection.immutable._
import scala.collection.mutable
import scala.io.Source
import scala.util.parsing.json.JSON
import scalaj.http.{HttpOptions, Http}

/**
 * Created by yairgrosu on 8/18/14.
 */



trait Stemmer{
    def stemIt(aWord: String): String
}


trait Analyzer extends Stemmer{
//    this: Stemmer =>
    val delimiters:String
    val blackList:List[String]

    def tokenizeText(text:String):List[String] = {
        text.split(delimiters.toCharArray).filter(!blackList.contains(_)).filter(_.length()>=2).toList
    }
}


trait CrawlerServiceComponent {
    def crawlerService:CrawlerService

    trait CrawlerService{
//        def analyzeDocument: String => List[String]
//        def termScoreMap: List[String] => Map[String, Double]
        def docScoredDocuments(docId:String): Map[String, Double]
        def loadCorpus(newDocs:Map[String, String])
    }
}

trait TermFreqAlgo{
    def termsScores(terms:Map[String, Int]):Map[String,Double]
}


trait TermFreqAlgoAugmented extends TermFreqAlgo {
    def termsScores(docTerms: Map[String, Int] ):  Map[String,Double] = {
        val maxCount = docTerms.maxBy(_._2)._2
        docTerms.map({case (term,termCount)=> {
            val tf = 0.5*(termCount)/(maxCount)+0.5
            (term->tf)
        }} )
    }
}

trait DefaultCrawlerServiceComponent extends CrawlerServiceComponent{
    this: Analyzer with TermFreqAlgo=>


    def crawlerService = new DefaultCrawlerService()

    class DefaultCrawlerService extends CrawlerService{

//        val docsMap:mutable.Map[String, String] = new mutable.HashMap[String,String]()
        val docsTermsMap:mutable.Map[String, Map[String,Int]] = new mutable.HashMap[String, Map[String,Int]]()


        def docScoredDocuments(docId:String):Map[String, Double] = {
            val scoredTerms = termsScores(docsTermsMap(docId))
            val termsInDocsCounts = docsTermsMap.values.toList.flatten.groupBy(_._1).map({case(term, counts)=> { (term -> counts.length) }})
            val nDocs=docsTermsMap.size
            val idf = termsInDocsCounts.keySet.map(term => { (term -> (Math.log(nDocs/termsInDocsCounts(term)))) }).toMap

            scoredTerms.keySet.map(term=>{ (term->(scoredTerms(term)*idf(term)) )  }).toMap
        }

        def loadCorpus(docsToAdd: Map[String, String]) = {
            docsToAdd.filterKeys(!docsTermsMap.contains(_)).foreach({case(docId:String, text:String) => {
//                docsMap.put(docId,text)
                docsTermsMap.put(docId, tokenizeText(text).groupBy(stemIt(_)).map({ case (k, v) => (k -> v.length)}).toMap)

              } })
        }
    }

}

object Indexer {
    def main(args: Array[String]): Unit = {
        val jsonUrlPfx = "http://itunes.apple.com/lookup?id="
        val itemIds = List(429775439, 553834731, 529479190, 499511971, 513474544,
            506627515, 504720040, 447553564, 440045374, 512939461)

        val theList = args.length match{
            case 0 => {println ("using the internal sample IDs, enjoy.  You can add the path to file with ids");itemIds}
            case _ => {
                println(s"Taking IDs from the file at [$args(0)]")
                val ids = Source.fromFile(args(0)).getLines().map(line => line.toInt).toList
                println(s"Found ${ids.length} ids in the file, let's go")
                ids
            }
        }
        val docs:Map[String,String] = theList.par.map(item => {
            processDocFromUrl(jsonUrlPfx, item) match{case (k,Some(v))=>(k,v); case _ => {println(":-(");("","")}} })
          .toList.toMap.filterKeys(!_.isEmpty)
        println(s"size  ${docs.size}")

        val indexer = new DefaultCrawlerServiceComponent with Analyzer with TermFreqAlgoAugmented {
            val delimiters = "\t\n\f\r .,/\'\""
            val blackList = List("a", "the", "able", "ago", "also", "is", "are", "all", "so", "to", "it")

            def stemIt(aWord: String): String = {
                aWord.trim().toLowerCase()
            }
        }

        val crawler = indexer.crawlerService
        crawler.loadCorpus(docs)
        docs.keys.foreach(docId => println(s"\n$docId == ${crawler.docScoredDocuments(docId).toSeq.sortBy(_._2*(-1)).take(10)}" ))
        println("--")
        docs.keys.foreach(docId => println(s"\n$docId == ${crawler.docScoredDocuments(docId).toSeq.sortBy(_._2).take(10)}" ))
        println("--")

        List("553834731","506627515").map(docId=>println(s"\n\n$docId == ${crawler.docScoredDocuments(docId).toSeq.sortBy(_._2)}"))

    }

    val defOpt = List(HttpOptions.connTimeout(1000), HttpOptions.readTimeout(5000))

    def processDocFromUrl(theUrl: String, itemId: Long):(String, Some[String]) = {

        // just to verify parallelism, threading
        // println(s"Thread processDocFromUrl ${Thread.currentThread().getName}")
        val content = Http.get(theUrl + itemId).options(defOpt).asString
        println(theUrl + itemId)//+descFromJson(content))
        (s"$itemId"->descFromJson(content))
    }

    def descFromJson(js: String) = {
        val desc:String = JSON.parseFull(js) match {
            case Some(m: Map[String, Any]) => {
                m("results") match {
                    case res: List[Map[String, Any]] => {
                        res(0).get("description") match {case Some(s:String) =>s}
                    }
                }
            }
        }
        Some(desc)
    }


}