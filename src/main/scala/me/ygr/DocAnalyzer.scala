package me.ygr

/**
 * Created by yairgrosu on 7/30/14.
 */


class DocAnalyzer(val stemmingOps: List[StemStep], val delims: String) {

    def  analyze(docText: Option[String], id: Long): (Long, Map[String, Int]) = {
        val wordsHist = docText.getOrElse("").split(delims.toCharArray).groupBy(aWord => stemTheWord(aWord)).map({ case (k, v) => (k -> v.length)})
        (id -> wordsHist.filterKeys(_.length > 1))
    }

    def stemTheWord(word: String) = {
        val result = stemmingOps.foldLeft(word)((s: String, stemStep: StemStep) => stemStep.step(s))
        result
    }
}

trait StemStep {
    val step: String => String
}

object StemTrim extends {
    val step: String => String = _.trim
} with StemStep {}

object StemLower extends {
    val step: String => String = _.toLowerCase
} with StemStep {}

trait StopWordCleaner {
    def stopWords: Set[String]

    def process(str: String): String = {
        if (stopWords.contains(str)) "" else str
    }
}

case class StemStopper(val theStopWords: Set[String]) extends StopWordCleaner with StemStep {
    val step: String => String = process(_)

    def stopWords: Set[String] = theStopWords
}

trait TrDocAnalyzer {

    def theStemmingOps: List[StemStep]

    def delimiters: String

    def analyzer(): DocAnalyzer = new DocAnalyzer(theStemmingOps, delimiters)
}

