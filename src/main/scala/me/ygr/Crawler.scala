package me.ygr

import scala.util.parsing.json._
import scalaj.http.{Http, HttpOptions}

object Crawler extends TrDocAnalyzer{
    def theStemmingOps: List[StemStep] = {
        val stopWords: Set[String] = Set("a", "the", "able", "ago", "also", "is", "are", "all", "so", "to", "it")
        List(StemTrim, StemLower, new StemStopper(stopWords))
    }

    def delimiters: String = "\t\n\f\r .,/\'\""

    val defOpt = List(HttpOptions.connTimeout(1000), HttpOptions.readTimeout(5000))


    def processDocFromUrl(theUrl: String, itemId: Long) = {
        // just to verify parallelism, threading
        // println(s"Thread processDocFromUrl ${Thread.currentThread().getName}")
        val content = Http.get(theUrl + itemId).options(defOpt).asString

        analyzer().analyze(descFromJson(content), itemId)
    }

    def descFromJson(js: String) = {
        val desc:String = JSON.parseFull(js) match {
            case Some(m: Map[String, Any]) => {
                m("results") match {
                    case res: List[Map[String, Any]] => {
                        res(0).get("description") match {case Some(s:String) =>s}
                    }
                }
            }
        }
        Some(desc)
    }


}
