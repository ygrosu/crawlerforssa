package me.ygr

/**
 * Created by yairgrosu on 7/30/14.
 */

trait TfAlgorithm {
    def calculate(docTerms:Map[String, Int]):Map[String,Double]
}

trait TfAlgorithmLog extends TfAlgorithm {
    def calculate(docTerms: Map[String, Int] ): Map[String,Double] = {
        docTerms.map({case (term,termCount)=> {
            // Keep separate for debugging, do not inline
            val tf = Math.log(1+termCount)
            (term->tf)
        }} )
    }
}

trait TfAlgorithmAugmented extends TfAlgorithm {
    def calculate(docTerms: Map[String, Int] ): Map[String,Double] = {
        val maxCount = docTerms.maxBy(_._2)._2
            docTerms.map({case (term,termCount)=> {
            // Keep separate for debugging, do not inline
            val tf = 0.5*(termCount)/(maxCount)+0.5
            (term->tf)
        }} )
    }
}

trait DocIndexer {
    this : TfAlgorithm =>

    def processDocTf(docId:Long, docTerms: Map[String, Int]) : Map[String,Double]  = calculate(docTerms)

    def processCorpusIdf(allDocs:List[Map[String, Int]]) : Map[String,Double] = {
        val nDocs = (allDocs.length).toDouble

        val termsCounts = allDocs.flatten.groupBy(_._1).map({case(term, counts)=> { (term -> counts.length) }})

        termsCounts.keySet.map(term => { (term -> (Math.log(nDocs/termsCounts(term)))) }).toMap
    }


    def docTermsScores(docTermsTf: Map[String,Double], courpusIdf: Map[String,Double]) : Map[String,Double] = {
        docTermsTf.keySet.map(term=> (term->(docTermsTf(term)*courpusIdf(term))) ).toMap
    }

}
